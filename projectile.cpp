#include "projectile.h"


Projectile::Projectile(SpriteSheet *sheet) : Drawable(), Movable() {
    this->sheet = sheet;
    proRect = new SDL_Rect();
    proRect->x = 0;
    proRect->y = 0;
    proRect->w = 10;
    proRect->h = 10;
}

void Projectile::draw(SDL_Renderer *renderer) {
    sheet->drawFrame("ispaljen", 1, proRect, renderer);
}

void Projectile::move(int dx, int dy) {
    proRect->x = dx;
    proRect->y = dy;
}

void Projectile::move() {
    proRect->y += 1;

}

