#include "player.h"

Player::Player(vector<Sprite*> sprites) : Drawable(), Movable(), KeyboardEventListener() {
    this->sprites = sprites;
}

void Player::draw(SDL_Renderer *renderer) {
    sprites[0]->draw(renderer);
}

void Player::move() {
    sprites[0]->move();
}

void Player::move(int dx, int dy) {
    sprites[0]->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event) {
    if(event.type == SDL_KEYDOWN) {
        if(event.keysym.sym == SDLK_LEFT) {
            sprites[0]->setState(sprites[0]->getState()|Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT) {
            sprites[0]->setState(sprites[0]->getState()|Sprite::RIGHT);
        } else if(event.keysym.sym == SDLK_UP) {
            sprites[0]->setState(sprites[0]->getState()|Sprite::UP);
        } else if(event.keysym.sym == SDLK_DOWN) {
            sprites[0]->setState(sprites[0]->getState()|Sprite::DOWN);
        }else if(event.keysym.sym==SDLK_SPACE){
            sprites[0]->hide();
        }
    } else if (event.type == SDL_KEYUP) {
        if(event.keysym.sym == SDLK_LEFT) {
            sprites[0]->setState(sprites[0]->getState()&~Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT) {
            sprites[0]->setState(sprites[0]->getState()&~Sprite::RIGHT);
        } else if(event.keysym.sym == SDLK_UP) {
            sprites[0]->setState(sprites[0]->getState()&~Sprite::UP);
        } else if(event.keysym.sym == SDLK_DOWN) {
            sprites[0]->setState(sprites[0]->getState()&~Sprite::DOWN);
        }else if(event.keysym.sym==SDLK_SPACE){
            sprites[0]->reveal();
        }

    }

    }
