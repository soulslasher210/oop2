#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <string>
#include "SDL.h"
#include "SDL_image.h"

#include "drawable.h"
#include "movable.h"

#include "spritesheet.h"
#include <vector>
#include "projectile.h"
using namespace std;

class Sprite : public Drawable, public Movable {
public:
    enum State:short int {STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8,A=10,
                     LEFT_UP=LEFT|UP|A, LEFT_DOWN=LEFT|DOWN|A,
                     RIGHT_UP=RIGHT|UP|A, RIGHT_DOWN=RIGHT|DOWN|A,};
private:
    short int state;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame;
    int frameCounter;
    int frameSkip;
    int health;
    bool hidden;

public:
    Sprite(SpriteSheet *sheet,int health, int width=64, int height=64, bool hidden=false);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    virtual void hide();
    virtual void reveal();
};

#endif // SPRITE_H_INCLUDED
