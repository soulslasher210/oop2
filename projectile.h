#ifndef PROJECTILE_H_INCLUDED
#define PROJECTILE_H_INCLUDED
#include <string>
#include "SDL.h"
#include "SDL_image.h"
#include "drawable.h"
#include "movable.h"

#include "spritesheet.h"
class Projectile : public Drawable, public Movable{
private:
    SpriteSheet *sheet;
    SDL_Rect *proRect;
public:
    Projectile(SpriteSheet *sheet);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
};

#endif // PROJECTILE_H_INCLUDED
